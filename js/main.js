/* Обьясните своими словами, как вы понимаете асинхронность в Javascript
ДжэЭс однопоточные, но в его окружении есть некоторые программы, которые позволяют выполнять асинхронные действия */

const btn = document.getElementById("findIPBtn");

btn.addEventListener("click", () => {
  requestIP().then((data) => {
    sendIP(data.ip)
      .then((data) => {
        const root = document.getElementById("root");
        root.innerHTML = "";

        const arrayInfo = ["country", "regionName", "city"];

        arrayInfo.forEach((info) => {
          if (data[info]) {
            const li = document.createElement("li");
            li.textContent = data[info];
            root.append(li);
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

async function requestIP() {
  return await fetch("https://api.ipify.org/?format=json")
    .then((data) => {
      return data.json();
    })
    .catch((err) => {
      console.log(err);
    });
}

async function sendIP(ip) {
  return await fetch("http://ip-api.com/json/" + ip)
    .then((data) => {
      return data.json();
    })
    .catch((err) => {
      console.log(err);
    });
}
